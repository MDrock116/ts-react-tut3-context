import React from "react";

const { Provider, Consumer } = React.createContext(""); //without hooks
const Store = React.createContext({}); //with hooks

export function Parent(props) {  //without hooks
  const myVar = "Context w/o Hooks";
  return <Provider value={myVar}>
            {props.children}
        </Provider>;
}

export function Parent2(props) {  //with hooks, props holds list of child components
  //const myVar = "Context w/ Hooks"; //could have still used just text
  const myVar = {text: "Context w/ Hooks & obj variable"}
  return <Store.Provider value={myVar}>  /*wrap children w/ access to context variable*/
          {props.children}
        </Store.Provider>;
}

export function Child() {  //without hooks
  return <Consumer>
            {(text) => <div>{text}</div>}
        </Consumer>
}
export function ChildWithHook(){ //with hooks (eliminate need for consumer & pass down as callback function)
  const hook = React.useContext(Store)
  return <div>{hook.text}</div>
}