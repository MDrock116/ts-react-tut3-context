//https://www.udemy.com/course/typescript-with-react-hooks-and-context/learn/lecture/13818724#overview
import React from "react";
import {Parent, Child, ChildWithHook, Parent2} from "./MyFunctions";

function App() {
  return (
    <span>
        <Parent>
            <Child />  {/*this a prop to parent tag*/}
        </Parent>
        <br />
        <Parent2>
            <ChildWithHook />   {/*this a prop to parent tag*/}
        </Parent2>
    </span>

  );
}

export default App;
